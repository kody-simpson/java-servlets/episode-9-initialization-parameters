import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//You can put init parameters using the annotations if you would like too
@WebServlet(name = "Servlet2", urlPatterns = "/hello2", initParams = {@WebInitParam(name = "copyrightYear", value = "2019")})
public class Servlet2 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Get the servlet init parameter from the ServletConfig rather than the context
        //only this servlet can access the init param assigned to it
        String copyrightYear = getServletConfig().getInitParameter("copyrightYear");

        if (copyrightYear != null){
            System.out.println("Copyright year: " + copyrightYear);
        }

    }
}
